<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/', function () {
//     return view('home');
// });
Route::get('/', 'productController@home');


Route::get('/about', function () {
    return view('about');

    Route::get('/aov', function () {
        return view('aov');
      
});
// Route::get('/products', function () {
//     return view('product');
// });
// Route::get('/product/id/{id}', function () {
//     return view('single-product');
// });
Route::get('/products', 'productController@index');
Route::get('/products/search', 'productController@search');
Route::get('/product/id/{id}', 'productController@single');
Route::get('/cart', 'cartController@index');
Route::post('/cart/create', 'cartController@create');
Route::post('/cart/update', 'cartController@update');
Route::post('/cart/finish', 'cartController@finish');
// Route::get('/cart', function () {
//     return view('cart');
// });




Route::get('/contact', function () {
    return view('contact');
});
Route::post('/contact/store', 'contactController@store');


// Route::get('/login', function () {
//     return view('auth.login');
// });
// Route::get('/register', function () {
//     return view('auth.register');
// });

//----------------------------- ADMIN PAGE -------------------------
Auth::routes();
Route::get('/admin/home', 'AdminHomeController@index')->name('home');
Route::get('/admin/about', 'AdminAboutController@index')->name('about');
Route::get('/admin/product', 'AdminProductController@index')->name('product');
Route::get('/admin/product/create', 'AdminProductController@create')->name('product');
Route::post('/admin/product/store', 'AdminProductController@store');
Route::get('/admin/product/delete/{id}','AdminProductController@delete');
Route::get('/admin/product/edit/{id}','AdminProductController@edit');
Route::post('/admin/product/update','AdminProductController@update');
Route::get('/admin/order', 'AdminOrderController@index')->name('order');
Route::get('/admin/order/confirm/{id}', 'AdminOrderController@confirm');
Route::get('/admin/order/delete/{id}','AdminOrderController@delete');
Route::get('/admin/contact', 'AdminContactController@index')->name('contact');
Route::get('/admin/contact/delete/{id}','AdminContactController@delete');
Route::get('/admin/aov/aov/{id}','AdminContactController@aov');







                        // Route::get('/admin/home', function () {
                        //     return view('admin.home');
                        // });
                        // Route::get('/admin/product', 'AdminProductController@index');
                        // Route::resource('/admin/product', 'AdminProductController');
                         Route::resource('/aov/aov', 'AdminReportController');