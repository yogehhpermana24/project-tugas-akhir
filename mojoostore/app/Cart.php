<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'cart';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update';

    function produk() {
        return $this->hasMany('App\Produk');
    }

}
