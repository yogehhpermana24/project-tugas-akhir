<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class contactController extends Controller
{
    //
    
    public function store(Request $request)
    {
        
        DB::table('contact')->insert([
            'nama' => $request->nama,
            'email' => $request->email,
            'nomor_hp' => $request->nomor_hp,
            'pesan' => $request->pesan
        ]);
            return redirect('/');
    }

}