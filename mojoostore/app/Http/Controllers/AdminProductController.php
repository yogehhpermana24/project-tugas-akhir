<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AdminProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

    	$produk = DB::table('produk')->get();
 
    	// mengirim data product ke view index
    	return view('admin.product',['produk' => $produk]);
        // return view('admin.product');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        return view('admin.product_create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'gambar' => 'required|file|max:1000'
        ]);
        $uploadedFile = $request->file('gambar');
        // $path = $uploadedFile->store('public/product');
        // $uploadedFile->move($path, $uploadedFile->getClientOriginalName());
        $path = '/product/'.time()."_".$uploadedFile->getClientOriginalName();
        $uploadedFile->move( public_path('product/'), time()."_".$uploadedFile->getClientOriginalName());

        DB::table('produk')->insert([
            'nama_produk' => $request->nama_produk,
            'ukuran' => $request->ukuran,
            'harga' => $request->harga,
            'stock' => $request->stock,
            'gambar' => $path,
            'warna' => $request->warna
        ]);
        return redirect('/admin/product/create');
    }

    public function delete($id)
    {
        $gambar = DB::table('produk')->where('id',$id)->first();
        unlink(public_path($gambar->gambar));
        DB::table('produk')->where('id',$id)->delete();
        DB::table('cart')->where('id_produk',$id)->delete();
        return redirect('/admin/product');
    }

    public function edit($id)
    {
        $produk = DB::table('produk')->where('id',$id)->get();
        return view('admin.product_edit',['produk' => $produk]);
    }

    public function update(Request $request)
    {
        $uploadedFile = $request->file('gambar');
        if( !empty($uploadedFile) ){
            $this->validate($request, [
                'gambar' => 'required|file|max:1000'
            ]);
            $uploadedFile = $request->file('gambar');
            // $path = $uploadedFile->store('/public/product');
            unlink(public_path($request->nama_gambar));
            $path = '/product/'.time()."_".$uploadedFile->getClientOriginalName();
            $uploadedFile->move( public_path('product/'), time()."_".$uploadedFile->getClientOriginalName());
            // update data produk
            DB::table('produk')->where('id',$request->id)->update([
                'nama_produk' => $request->nama_produk,
                'ukuran' => $request->ukuran,
                'harga' => $request->harga,
                'stock' => $request->stock,
                'gambar' => $path,
                'warna' => $request->warna
            ]);
        }else if(empty($uploadedFile) ){
            DB::table('produk')->where('id',$request->id)->update([
                'nama_produk' => $request->nama_produk,
                'ukuran' => $request->ukuran,
                'harga' => $request->harga,
                'stock' => $request->stock,
                'warna' => $request->warna
            ]);
        }
        // alihkan halaman ke halaman produk
        return redirect('/admin/product/edit/'.$request->id);
    }
    




}
