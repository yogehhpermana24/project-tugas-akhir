<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\App\Produk;
use Carbon\Carbon;

class AdminOrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $cart = DB::table('cart')
                ->join('produk', 'cart.id_produk', '=', 'produk.id')
                ->select('cart.*', 'produk.*')
                ->groupBy('cart.invoice')
                ->having('cart.nama', '!=', '')
                ->orderBy('cart.status', 'ASC')
                ->orderBy('cart.id', 'DESC')
                ->get();

        $produk = DB::table('cart')
                ->join('produk', 'cart.id_produk', '=', 'produk.id')
                ->select('cart.*', 'produk.*')
                ->get();
    	return view('admin.order',['cart' => $cart, 'produk'=>$produk]);
    }

    public function delete($id)
    {
        $order = DB::table('cart')
                    ->join('produk', 'cart.id_produk', '=', 'produk.id')
                    ->select('cart.*', 'produk.*')
                    ->where('cart.invoice',$id)
                    ->get();
        foreach($order as $pro){
            $stock = $pro->jumlah + $pro->stock;
            DB::table('produk')->where('id',$pro->id)->update([
                'stock' => $stock,
            ]);
        }
        DB::table('cart')->where('invoice',$id)->delete();
        return redirect('/admin/order');
    }

    public function confirm($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = Carbon::now()->toDateTimeString();
        DB::table('cart')->where('invoice',$id)->update([
            'status' => 1,
            'updated_at' => $now
        ]);
        return redirect('/admin/order');
    }



}
