<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class productController extends Controller
{
    public function index()
    {
    	$produk = DB::table('produk')->where('stock','!=',0)->get();
    	return view('product',['produk' => $produk]);
    }
// Tampilan Home
    public function home()
    {
    	$produk = DB::table('produk')->where('stock','!=',0)->orderBy('id','DESC')->paginate(7);
    	return view('home',['produk' => $produk]);
    }
//     memanggil database detail produk
    public function single($id)
    {
    	$produk = DB::table('produk')->where('id',$id)->first();
        $cek = DB::table('cart')
                ->where('nama','')
                ->where('status',0)
                ->where('id_produk',$id)
                ->count();
        $cek_invoice = DB::table('cart')
                ->where('nama','')
                ->count();
        $cart = DB::table('cart')
                ->where('nama','')
                ->where('status',0)
                ->where('id_produk',$id)
                ->first();
                if( DB::table('cart')->count() == 0 ){
                        $last = 0;                        
                }else{
                        $last = DB::table('cart')->orderBy('id', 'DESC')->first();
                        $last = $last->id;
                }
                return view('single-product',['p' => $produk, 'cek' => $cek, 'invoice' => $cek_invoice, 'c' => $cart, 'last_id' => $last]);
        }
        // mencari dan menampilkan data berdasarkan request
        public function search(Request $request)
        {
                $cari = $request->s;
                $produk = DB::table('produk')->where('nama_produk', 'like', '%'.$cari.'%')->where('stock','!=',0)->paginate();    
                return view('product',['produk' => $produk]);
        }





    //
}
