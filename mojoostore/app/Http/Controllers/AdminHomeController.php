<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AdminHomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        function bulan_indonesia($bulan){
            switch($bulan){
                case 1:
                return 'Jan';
                break;
                case 2:
                return 'Feb';
                break;
                case 3:
                return 'Mar';
                break;
                case 4:
                return 'Apr';
                break;
                case 5:
                return 'Mei';
                break;
                case 6:
                return 'Jun';
                break;
                case 7:
                return 'Jul';
                break;
                case 8:
                return 'Agu';
                break;
                case 9:
                return 'Sep';
                break;
                case 10:
                return 'Okt';
                break;
                case 11:
                return 'Nov';
                break;
                case 12:
                return 'Des';
                break;
            }
        }

        $year = [];
        $bulan = [];
        $jumlah = [];
        $month = [];
        for( $arr = 12;$arr > 0;$arr-- ){
            $year = date('Y', strtotime('-'.$arr.' months'));
            array_push($bulan, bulan_indonesia(date('n', strtotime('-'.$arr.' months'))).' '.$year   );

            $month = date('m', strtotime('-'.$arr.' months'));
            $cart = DB::table('cart')
                    ->select('jumlah')
                    ->where('status', '=', '1')
                    ->whereYear('updated_at', '=', $year)
                    ->whereMonth('updated_at', '=', $month)
                    ->groupBy('invoice')
                    ->get();
            array_push($jumlah, count($cart) );
        }

        $bulan_ini = DB::table('cart')
                    ->select('id')
                    ->where('status', '=', '1')
                    ->whereYear('updated_at', '=', date('Y') )
                    ->whereMonth('updated_at', '=', date('m') )
                    ->groupBy('invoice')
                    ->get();
        $bulan_ini = count($bulan_ini);                    

        $total = DB::table('cart')
                    ->where('status', '=', '1')
                    ->groupBy('invoice')
                    ->get();
        $total = count($total);

        return view('admin.home', ['bulan' => $bulan, 'jumlah' => $jumlah, 'bulan_ini' => $bulan_ini, 'total' => $total]);
    }

    
        
}