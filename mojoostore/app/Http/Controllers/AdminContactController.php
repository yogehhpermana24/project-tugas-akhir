<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $contact = DB::table('contact')->get();
 
    	// mengirim data product ke view index
    	return view('admin.contact',['contact' => $contact]);
        // return view('admin.product');
    }

    public function delete($id)
    {
        // menghapus data pegawai berdasarkan id yang dipilih
        DB::table('contact')->where('id',$id)->delete();
            
        // alihkan halaman ke halaman pegawai
        return redirect('/admin/contact');
    }



}
