<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class cartController extends Controller
{
    public function index()
    {
        $cart = DB::table('cart')
            ->join('produk', 'cart.id_produk', '=', 'produk.id')
            ->where('cart.status', 0)
            ->where('cart.nama','')
            ->get();
        $inv = DB::table('cart')
            ->join('produk', 'cart.id_produk', '=', 'produk.id')
            ->where('cart.status', 0)
            ->where('cart.nama','')
            ->first();
    	return view('cart',['cart' => $cart, 'inv' => $inv]);
    }

    public function create(Request $request)
    {
        if( $request->invoice == 0 ){
        	$inv = DB::table('cart')->where('status',0)->orderBy('id','DESC')->first();
            $invoice = $inv->invoice;
        }else{
            $invoice = $request->invoice;
        }
        DB::table('cart')->insert([
            'invoice' => $invoice,
            'id_produk' => $request->id_produk,
            'jumlah' => $request->jumlah
        ]);
        $pro = DB::table('produk')->where('id', $request->id_produk)->first();
        $total_stok = $pro->stock - $request->jumlah;
        DB::table('produk')->where('id', $request->id_produk)->update([
            'stock' => $total_stok,
        ]);
        return redirect('/products');
    }

    public function update(Request $request)
    {
        if( $request->invoice == 0 ){
        	$inv = DB::table('cart')->where('status',0)->orderBy('id','DESC')->first();
            $invoice = $inv->invoice;
        }else{
            $invoice = $request->invoice;
        }
        DB::table('cart')->where('id_produk',$request->id_produk)->where('status', 0)->update([
            'jumlah' => $request->jumlah
        ]);
        $pro = DB::table('produk')->where('id', $request->id_produk)->first();
        $total_stok = $pro->stock - $request->jumlah;
        DB::table('produk')->where('id', $request->id_produk)->update([
            'stock' => $total_stok,
        ]);
        return redirect('/products');
    }

    public function finish(Request $request)
    {
        DB::table('cart')->where('invoice',$request->invoice)->where('status', 0)->update([
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'nomor_hp' => $request->nomor_hp
        ]);
        return redirect('/products');
    }


}
