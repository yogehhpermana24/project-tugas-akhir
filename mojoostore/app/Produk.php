<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Produk extends Model
{
    protected $table = 'produk';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update';

    function cart() {
        return $this->hasMany('App\Cart');
    }

}
