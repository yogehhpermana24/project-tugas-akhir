@include('header')


<div class="container">
    <h1>Cart</h1>

<!-- untuk Menampilkan pesanan yang telah  di pilih user pada form produk -->
    @if( count($cart) > 0 )
    <div class="col-sm-12 text-center">
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Merk</th>
                    <th>Size</th>
                    <th>Color</th>
                    <th>Unit Price</th>
                    <th>Qty</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
            @php
                $total = 0;
            @endphp
            @foreach($cart as $no => $c)
                <tr>
                    <td>{{ $no+1 }}</td>
                    <td>{{ $c->nama_produk }}</td>
                    <td>{{ $c->ukuran }}</td>
                    <td>{{ $c->warna }}</td>
                    <td>{{ number_format($c->harga, 0, '', '.') }}</td>
                    <td>{{ $c->jumlah }}</td>
                    <td>Rp {{ number_format($c->jumlah * $c->harga, 0, '', '.') }}</td>
                </tr>
                @php
                    $total += $c->jumlah * $c->harga;
                @endphp
            @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="5"><b>Jumlah</b></td>
                    <td><b>Rp {{ number_format($total, 0, '', '.') }}</b></td>
                </tr>
            </tfoot>
        </table>
        </div>

        <hr>

    <form action="{{ url('/cart/finish') }}" role="form" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ method_field('post') }}

        <div class="clearfix"></div>
        <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for=""><b>Isi Data Berikut:</b></label>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Nama Lengkap" name="nama" required>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="exampleFormControlInput2" placeholder="Alamat Pengiriman" name="alamat" required>
            </div>
            <div class="form-group">
                <input type="number" class="form-control" id="exampleFormControlInput3" placeholder="No. HP" name="nomor_hp" required>
            </div>
            <div class="form-group">
            </div>
        </div>
        <div class="col-sm-6">
            <div style="background-color: #f6f6f6;padding: 25px 25px 15px 25px;border-radius: 6px;">
                <p>Silahkan lakukan pembayaran ke rekening <br><b>BCA 781264783 an Mojoo Store</b></p>
                <p>
                    Untuk konfirmasi pembayaran,
                    <br>hubungi kami via <b>Whatsapp <a href="https://wa.me/6289693648214" target="_blank">089693648214</a></b>
                    <br>dengan kode Invoice <b>#MS{{ $inv->invoice }}</b>
                    <br>sebesar <b>Rp {{ number_format($total, 0, '', '.') }}</b>
                </p>
                <p>Terima kasih :)</p>
            </div>
        </div>
        </div>
        <div class="clearfix" style="margin-bottom: 25px"></div>
        <input type="hidden" name="invoice" value="{{ $inv->invoice }}">
        <button type="submit" class="btn btn-primary">Bayar</button>
    </form>

    </div>

    @else
        <p class="text-center">Maaf, kamu belum ada belanjaan. Silahkan belanja dulu ya :)</p>
        <p class="text-center">Yuk, lihat <b><a href="{{ url('/products') }}">Koleksi Sepatu Mojoo Store</a></b></p>
    @endif


</div>





@include('footer')