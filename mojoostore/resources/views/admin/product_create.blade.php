@extends('layouts.app')

@section('content')

    <div class="col-md-12">
        <h1>PRODUCT</h1>

<!-- VIEW -->
<a class="btn btn-primary" href="{{ url('/admin/product') }}">Lihat Produk</a>


<!-- VIEW -->

        <!-- CREATE -->
        <form action="{{ url('/admin/product/store') }}" role="form" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('post') }}
        <div class="form-group">
            <label for="foto_produk">Foto Produk</label>
            <input type="file" class="form-control" name="gambar" id="foto_produk">
        </div>
        <div class="form-group">
            <label for="merk">Merk</label>
            <input type="text" class="form-control" placeholder="Merk" name="nama_produk" id="merk">
        </div>
        <div class="form-group">
            <label for="size">Ukuran</label>
            <input type="text" class="form-control" placeholder="Ukuran" name="ukuran" id="size">
        </div>
        <div class="form-group">
            <label for="color">Warna</label>
            <input type="text" class="form-control" placeholder="Warna" name="warna" id="color">
        </div>
        <div class="form-group">
            <label for="price">Harga</label>
            <input type="number" class="form-control" placeholder="Harga" name="harga" id="price">
        </div>
        <div class="form-group">
            <label for="qty">Stock</label>
            <input type="number" class="form-control" placeholder="Stock" name="stock" id="qty">
        </div>

            <input type="submit" value="SIMPAN" class="btn btn-primary">
        </form>
        <!-- CREATE -->
    </div>

@endsection
