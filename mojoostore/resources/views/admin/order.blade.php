@extends('layouts.app')

@section('content')

    <div class="col-md-12">
        <h1>ORDER</h1>

<!-- VIEW -->
<div class="table-responsive">
<!-- untuk tampilkan produk di tabel cart yang siap dikonfirmasi -->
<table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Invoice</th>
                    <th>Nama Pemesan</th>
                    <th>Alamat</th>
                    <th>No. Telpon</th>
                    <th>Produk</th>
                    <th>Harga</th>
                    <th>Status</th>
                    <th>Action</th>
                    
                </tr>
            </thead>
            <tbody>
            @foreach($cart as $no => $p)
                <tr>
                    <td>{{ $no+1 }}</td>
                    <td data-invoice="{{ $p->invoice }}">#MS{{ $p->invoice }}</td>
                    <td>{{ $p->nama }}</td>
                    <td>{{ $p->alamat }}</td>
                    <td>{{ $p->nomor_hp }}</td>
                    <td>
                        @php
                            $total = 0;
                            $batas = 0;
                            $price = [];
                        @endphp
                        @if($produk->where('id','=',$p->id_produk))
                            @foreach($produk as $pro)
                                @if($pro->invoice == $p->invoice)
                                {!! $batas > 0 ? '<br>' : '' !!}
                                {{ $pro->jumlah.'x'.$pro->nama_produk.' ('.$pro->ukuran.')' }}
                                @php
                                    $price[] = number_format($pro->jumlah * $pro->harga, 0, '', '.');
                                    $total += $pro->jumlah * $pro->harga;
                                    $batas++;
                                @endphp
                                @endif
                            @endforeach
                        @endif
                        <div style="display: none;" id="harga">{!! implode('<br>', $price) !!}</div>
                    </td>
                    <td>{{ number_format($total, 0, '', '.') }}</td>
                    <td>{!! ($p->status == 0 ? '<a href="#" data-toggle="modal" data-target="#myModal">Belum</a>':'Sudah') !!}</td>
                    <td><a data-toggle="modal" data-target="#myModal" href="#" data-id="{{ url('/admin/order/delete/').'/'.$p->invoice }}">Hapus</a></td>
                </tr>
            @endforeach
            </tbody>
            
        </table>
</div>
<!-- VIEW -->


    </div>

@endsection
