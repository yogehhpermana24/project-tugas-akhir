@extends('layouts.app')

@section('content')

    <div class="col-md-12">
        <h1>PRODUCT</h1>

<!-- VIEW -->
<a class="btn btn-primary" href="{{ url('/admin/product/create') }}">Tambah Produk</a>
<div class="clearfix" style="margin-top: 10px"></div>
<div class="table-responsive">
<table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Merk</th>
                    <th>Size</th>
                    <th>Color</th>
                    <th>Qty</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach($produk as $no => $p)
                <tr>
                    <td>{{ $no+1 }}</td>
                    <td>{{ $p->nama_produk }}</td>
                    <td>{{ $p->ukuran }}</td>
                    <td>{{ $p->warna }}</td>
                    <td>{{ $p->stock }}</td>
                    <td>{{ number_format($p->harga, 0, '', '.') }}</td>
                    <td>
				        <a href="{{ url('/admin/product/edit').'/'.$p->id }}">Edit</a>
				            |
				        <a data-toggle="modal" data-target="#myModal" href="#" data-id="{{ url('/admin/product/delete').'/'.$p->id }}">Hapus</a>
			        </td>
                </tr>
            @endforeach
            </tbody>
        </table>
</div>
<!-- VIEW -->


    </div>

@endsection
