@extends('layouts.app')

@section('content')

<div class="col-md-12" style="margin-top: 45px;margin-bottom: 20px;">
    <span style="padding: 10px 20px;background-color: #fff;font-size: 18pt;font-weight: 700;">Total: {{ $total }}</span>
    <span style="padding: 10px 20px;background-color: #fff;font-size: 18pt;font-weight: 700;">Bulan Ini: {{ $bulan_ini }}</span>
</div>

<div class="col-md-12" style="margin-bottom: 75px;background-color: #fff;">
    <canvas id="myChart"
        data-bulan="{{ implode(', ', $bulan) }}"
        data-order="{{ implode(', ', $jumlah) }}" >
    </canvas>
</div>

@endsection
