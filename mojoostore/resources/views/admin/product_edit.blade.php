@extends('layouts.app')

@section('content')

    <div class="col-md-12">
        <h1>PRODUCT</h1>

<!-- VIEW -->
<a class="btn btn-primary" href="{{ url('/admin/product') }}">Lihat Produk</a>


<!-- VIEW -->

        <!-- CREATE -->
        <form action="{{ url('/admin/product/update') }}" role="form" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('post') }}


@foreach($produk as $p)

        <div class="clearfix" style="margin-top: 15px"></div>
        <div class="col-sm-3">
            <img src="{{ url($p->gambar) }}" alt="" class="img-fluid">
            <input type="hidden" name="nama_gambar" value="{{ $p->gambar }}">
        </div>
        <div class="clearfix"></div>

        <div class="form-group">
            <label for="foto_produk">Foto Produk</label>
            <input type="file" class="form-control" name="gambar" id="foto_produk">
        </div>
        <div class="form-group">
            <label for="merk">Nama Produk</label>
            <input type="text" class="form-control" placeholder="Merk" name="nama_produk" id="merk" value="{{ $p->nama_produk }}">
        </div>
        <div class="form-group">
            <label for="size">Ukuran</label>
            <input type="text" class="form-control" placeholder="Ukuran" name="ukuran" id="size" value="{{ $p->ukuran }}">
        </div>
        <div class="form-group">
            <label for="color">Warna</label>
            <input type="text" class="form-control" placeholder="Warna" name="warna" id="color" value="{{ $p->warna }}">
        </div>
        <div class="form-group">
            <label for="price">Harga</label>
            <input type="number" class="form-control" placeholder="Harga" name="harga" id="price" value="{{ $p->harga }}">
        </div>
        <div class="form-group">
            <label for="qty">Stock</label>
            <input type="number" class="form-control" placeholder="Stock" name="stock" id="qty" value="{{ $p->stock }}">
        </div>
        <input type="hidden" name="id" value="{{ $p->id }}"> <br/>

@endforeach

            <input type="submit" value="SIMPAN" class="btn btn-primary">
        </form>
        <!-- CREATE -->
    </div>

@endsection
