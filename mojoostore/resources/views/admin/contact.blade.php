@extends('layouts.app')

@section('content')

    <div class="col-md-12">
        <h1>CONTACT</h1>
        <!-- VIEW -->
        <div class="table-responsive">
<table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Lengkap</th>
                    <th>No. HP</th>
                    <th>Email</th>
                    <th>Pesan</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($contact as $no => $p )
                <tr>
                    <td>{{ $no+1 }}</td>
                    <td>{{ $p->nama }}</td>
                    <td>{{ $p->nomor_hp }}</td>
                    <td>{{ $p->email }}</td>
                    <td>{{ $p->pesan }}</td>
                    <td><a data-toggle="modal" data-target="#myModal" href="#" data-id="{{ url('/admin/contact/delete').'/'.$p->id }}">Hapus</a></td>
                </tr>
                @endforeach
              
            </tbody>
        </table>
        </div>

<!-- VIEW -->

    </div>

@endsection
