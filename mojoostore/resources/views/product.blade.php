@include('header')

<div class="container">
  <h1>Koleksi Sepatu Mojoo Store</h1>
</div>
<!-- untuk menampilkan produk -->
<div class="container products">
  <div class="row">

  @if( count($produk) > 0 )
  @foreach($produk as $p)
    <div class="col-sm-4" style="margin-bottom: 25px;">
      <a href="{{ url('/product/id').'/'.$p->id }} ">
        <img src="{{ $p->gambar }}" class="img-fluid" />
        <div class="clearfix" style="margin-bottom: 10px;"></div>
        <div class="text-center">
        <h3>{{ $p->nama_produk }}</h3>
          <b>Price: Rp {{ number_format($p->harga, 0, '', '.') }}<br>
          Size: {{ $p->ukuran }}<br>
          Color: {{ $p->warna }}</b>
        </div>
      </a>
    </div>
  @endforeach
  @else
  <!-- search tampil ketika stok kosong/habis -->
    <div class="col-sm-12">
    <div class="clearfix" style="margin-bottom: 25px;"></div>
    <div style="background-color: #f6f6f6;padding: 25px 25px 15px 25px;border-radius: 6px;">
      <p class="text-center">
          Maaf, Stok Kosong boossskuhhhh :'(
      @if (\Request::is('products/search'))
        <br>Silahkan cek Koleksi Sepatu lainnya
        <br><b><a href="{{ url('/products') }}">Koleksi Sepatu Mojoo Store</a></b>
      @else
        <br>Mohon tunggu produk terbaru kami besok!
        <br>Jangan lupa untuk cek terus website kami atau langsung ke toko <b><a href="{{ url('/contact') }}">Mojoo Store</a></b> :)
      @endif
      </p>
    </div>
    </div>
  @endif
  </div>
</div>

@include('footer')