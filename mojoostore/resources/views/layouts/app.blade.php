<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Mojoo Store</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{ asset('themify-icons/themify-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('charts/Chart.min.css') }}" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        .dasbor-item{text-align: center}
        .dasbor-item a{color: #151515;text-decoration: none;}
        .dasbor-item i{display: block;font-size: 34pt;}
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    Mojoo Store
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @auth
                        <!-- nyambung ke web.php -->
                            <li class="nav-item"><a class="nav-link" href="{{ route('home') }}">Dashboard</a></li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @endauth
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
        @auth
        <!-- admin -->
        <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Menu</div>
                    <div class="card-body">
                        <div class="row dasbor-item">
                            <div class="col-md-3">
                            <a href="{{ url('/admin/home') }}">
                                <i class="ti-help"></i>
                                HOME
                            </a>
                            </div>

                            <div class="col-md-3">
                            <a href="{{ url('/admin/product') }}">
                                <i class="ti-package"></i>
                                PRODUCT
                            </a>
                            </div>

                            <div class="col-md-3">
                            <a href="{{ url('/admin/order') }}">
                                <i class="ti-shopping-cart"></i>
                                ORDER
                                </a>
                            </div>

                            <div class="col-md-3">
                            <a href="{{ url('/admin/contact') }}">
                                <i class="ti-user"></i>
                                CONTACT
                            </a>
                            </div>
                        </div>


                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @endauth
                @if(count($errors) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
				</div>
				@endif
            @yield('content')
        @auth
        </div>
        </div>
        @endauth
        </main>
    </div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document" style="margin-top: 15vh;">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-bold"></h4>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.10.0.min.js" integrity="sha256-2+LznWeWgL7AJ1ciaIG5rFP7GKemzzl+K75tRyTByOE=" crossorigin="anonymous"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('charts/Chart.min.js') }}"></script>

  <!-- <script src="{{ asset('js/bootstrap.min.js') }}" defer></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script> -->

  <script>
(function($){

    var home_url = '{{ url('/') }}';


    if( $('#myChart')[0] ){
        $('#myChart').each(function () {

            var $bulan = this.getAttribute('data-bulan'),
                $order = this.getAttribute('data-order'),
                ctx = this.getContext('2d');
            
            $bulans = [];
            $orders = [];
            $.each($bulan.split(','), function(ind, elem){
                $bulans.push(elem)
            })

            $.each($order.split(','), function(ind, elem){
                $orders.push(elem)
            })

            // console.log($orders, $bulans)

            new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: $bulans,
                    datasets: [{
                        label: 'Data Penjualan',
                        backgroundColor: 'rgb(255, 99, 132)',
                        borderColor: 'rgb(255, 99, 132)',
                        data: $orders
                    }]
                },
                options: {}
            });
        });
    }






    $('#myModal').on('show.bs.modal', function (e) {
        var modal = $(this)
        var data_id = $(e.relatedTarget).data('id')
        if( data_id ){
            modal.find('.modal-title').text( "DELETE" )
            modal.find('.modal-body').html( "<p class=\"text-center\">Yakin akan menghapus Data ini?</p>" )
            modal.find('#save').attr( "onclick", "window.location.href='"+data_id+"'" )
        }else{
            var tr = $(e.relatedTarget).closest('tr')
            modal.find('#save').attr( "onclick", "window.location.href='"+home_url+'/admin/order/confirm/'+tr.find('td').eq(1).attr('data-invoice')+"'" )
            modal.find('.modal-title').text( tr.find('td').eq(1).text() )
            modal.find('.modal-body').html(
                '<div class="float-left">'
                + tr.find('td').eq(5).html()
                + '</div><div class="float-right">'
                + tr.find('#harga').html()
                + '</div><div class="clearfix"></div>'
                +'<hr><div class="float-left"><b>Total: </b></div><div class="float-right"><b>'
                +tr.find('td').eq(6).html()
                +'</b></div>'
            )
        }
    })
    $('#myModal').on('hidden.bs.modal', function (e) {
        var modal = $(this)
        modal.find('.modal-title').text( "" )
        modal.find('.modal-body').html( "" )
        modal.find('#save').removeAttr( "onclick" )
    })

    




})(jQuery);
</script>


</body>
</html>
