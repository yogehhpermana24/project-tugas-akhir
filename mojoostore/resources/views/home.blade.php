@include('header')

<div class="container" style="margin-top: 75px;">

    <h1 class="text-center" style="color: #fff;">HOT ITEM!</h1>

    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">

        @foreach( $produk as $urut => $pro )
        <div class="carousel-item {{ ($urut == 0 ? 'active':'') }}">
            <div class="col-sm-12 text-center">
            <div style="margin:0 auto;text-align: center;">
            <img src="{{ $pro->gambar }}" alt="{{ $pro->nama_produk }}" class="img-fluid">
            <div class="carousel-caption d-none d-md-block">
                <h3>{{ $pro->nama_produk }}</h3>
            </div>
            </div>
            </div>
        </div>
        @endforeach
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    </div>

</div>


@include('footer')
