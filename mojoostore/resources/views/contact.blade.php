@include('header')



<div class="container">
    <h1>Contact Us Mojoo Store</h1>
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque odio ad animi incidunt hic, id assumenda iste corrupti, nulla earum dignissimos deleniti cupiditate quasi similique beatae recusandae delectus provident? Reiciendis?</p>
<!-- google maps -->
    <div class="row">
    <div class="col-sm-12">
        <div class="col-sm-6" style="float: left;">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.8176148556727!2d109.3257763144536!3d-0.03407263555071862!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e1d58ffb59cbeb1%3A0xd0cf9c05e1740604!2sMojo+Store!5e0!3m2!1sid!2sid!4v1561041233881!5m2!1sid!2sid" width="600" height="450" frameborder="0" style="border:0;max-width: 100%;width: 100%;" allowfullscreen></iframe>
        </div>
        <!-- pengisian form pada contact us untuk kritik dan saran -->
        <div class="col-sm-6" style="float: left;">
        <form action="{{ url('/contact/store') }}" role="form" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <!-- isi tabel contact -->
        {{ method_field('post') }}
                <div class="form-group">
                    <label for="exampleFormControlInput1">Nama Lengkap</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Nama Lengkap" name="nama"<input type="text" name="nama" required>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">No. Handphone</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="No. Handphone" name="nomor_hp"<input type="text" name="nomor_hp" required>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Email</label>
                    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="username@email.com" name="email"<input type="text" name="email" required>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Pesan</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="pesan"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Kirim</button> 
            </form>
        </div>
    </div>
    </div>

</div>

@include('footer')