@include('header')

<div class="container">
  <h1>Koleksi Sepatu Mojoo Store</h1>
</div>
<!-- search produk berdasarkan nama -->
<div class="container">
  <div class="row">
Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eveniet dicta reiciendis adipisci laboriosam sapiente quis maiores dolorem rerum praesentium voluptate tempora debitis sint repudiandae ullam dolore dolores nostrum, omnis iure.
  @foreach($produk as $p)
    <div class="col-sm-4" style="margin-bottom: 25px;">
      <a href="{{ url('/product/id').'/'.$p->id }} ">
        <img src="{{ $p->gambar }}" class="img-fluid" />
        <div class="text-center">
        <h3>{{ $p->nama_produk }}</h3>
          <b>Price: Rp {{ number_format($p->harga, 0, '', '.') }}<br>
          Size: {{ $p->ukuran }}<br>
          Color: {{ $p->warna }}</b>
        </div>
      </a>
    </div>
  @endforeach

  </div>
</div>

@include('footer')