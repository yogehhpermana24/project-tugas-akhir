<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/custom.css') }}">
    <title>Mojoo Store</title>
</head>

@if (\Request::is('/'))  
<body style="background-image: url({{ asset('/gambar/latar2.jpg') }});background-size: cover;">
@else
<body>
@endif

<style>
.navbar a{color: #ccc!important;}
.navbar .nav-item.active a{color: #fff!important;}
.products a{color: #343a40!important;}
.nav-link.dropdown-toggle{color: #4bb85c!important;}
a.dropdown-item{color: #4bb85c!important;}
.dropdown-item:focus, .dropdown-item:hover{background-color: #4bb85c!important;color: #fff!important;}
</style>

<nav class="navbar navbar-toggleable-md navbar-dark" style="background-color: #343a40!important;margin-bottom: 25px;">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <!-- navigasi bar untuk menu -->
  <a class="navbar-brand" href="{{ url('/') }}">Mojoo Store</a>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item {{ (\Request::is('about') ? 'active':'') }}">
        <a class="nav-link" href="{{ url('/about') }}">About</span></a>
      </li>
      <li class="nav-item {{ (\Request::is('products') ? 'active':'') }}">
        <a class="nav-link" href="{{ url('/products') }}">Product</a>
      </li>
      <li class="nav-item {{ (\Request::is('contact') ? 'active':'') }}">
        <a class="nav-link" href="{{ url('/contact') }}">Contact Us</a>
      </li>
      <li class="nav-item {{ (\Request::is('cart') ? 'active':'') }}">
        <a class="nav-link" href="{{ url('/cart') }}">Cart</a>
      </li>
      <!-- <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Admin
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="{{ url('/login') }}">Login</a>
          <a class="dropdown-item" href="{{ url('/register') }}">Register</a>
        </div>
      </li> -->
    </ul>
    <form class="form-inline my-2 my-lg-0" method="GET" action="{{ url('/products/search') }}">
      <input class="form-control mr-sm-2" type="text" placeholder="Nama Product" name="s" value="{{ old('s') }}">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cari</button>
    </form>


    <div class="my-2 my-lg-0 nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Admin
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="{{ url('/login') }}">Login</a>
          <!-- <a class="dropdown-item" href="{{ url('/register') }}">Register</a> -->
        </div>
      </div>


  </div>
</nav>