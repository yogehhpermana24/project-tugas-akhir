@include('header')

<div class="container">
    <h1>Produk</h1>
    <div class="clearfix" style="margin-bottom: 25px;"></div>
    <div class="col-sm-12 text-center">
        <div class="col-md-6" style="float: left;">
            <img src="{{ $p->gambar }}" class="img-fluid" />
        </div>
        <!-- tampilkan detail barang -->
        <div class="col-md-6 text-left" style="float: left;">
            <h3>{{ $p->nama_produk }}</h3>
            Size: {{ $p->ukuran }}<br>
            Color: {{ $p->warna }} <br>
            Stock: {{ $p->stock }} <br>
            @if( $cek > 0 )
                <form action="{{ url('/cart/update') }}" role="form" method="post" enctype="multipart/form-data">
            @else
                <form action="{{ url('/cart/create') }}" role="form" method="post" enctype="multipart/form-data">
            @endif
            {{ csrf_field() }}
            {{ method_field('post') }}
            Qty: <input type="number" class="form-control" placeholder="1" max="{{ $p->stock }}" value="{{ ($cek > 0 ? $c->jumlah + 1 : '1')  }}" name="jumlah" data-harga="{{ $p->harga }}"><br>
            Price: Rp {{ number_format($p->harga, 0, '', '.') }}<br>
            @if( $invoice > 0 )
                <input type="hidden" name="invoice" value="0">
            @else
                <input type="hidden" name="invoice" value="{{ sprintf('%05u', $last_id + 1) }}">
            @endif
            <input type="hidden" name="id_produk" value="{{ $p->id }}">
            <button type="submit" class="btn btn-primary">Tambahkan ke Cart</button>
            </form>
        </div>
    </div>
</div>

@include('footer')